# Reporte de 20 de las consultorías de software.
## 1
### Logotipo:
![logo2](logos/logo2.jpg)
## Nombre: ITEXICO
### Sitio web:
* https://www.itexico.com/
### Ubicaciòn:
* https://g.page/improving-guadalajara?share

### Acerca de:
* Improving es una firma de servicios de TI completa que ofrece capacitación, consultoría, reclutamiento y servicios de proyectos. Nuestras soluciones y procesos innovadores han ayudado a cientos de clientes en todo el mundo a lograr sus objetivos comerciales tácticos y estratégicos en varias industrias, incluidos los servicios financieros, la atención médica, la energía, el comercio minorista, el gobierno y más.

### Servicios:
* Desarrollo de software
* Diseño de Producto Digital UI / UX
* Desarrollo de front-end
* Desarrollo de back-end
* Servicios de control de calidad y pruebas
* Desarrollo tecnológico de Microsoft
* Desarrollo de tecnología Java

### Presencia:
* https://twitter.com/iTexico
* https://www.facebook.com/itexico
* https://www.linkedin.com/company/itexico/
* https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw

### Ofertas Laborales:
* https://www.itexico.com/careers

### Blog de ingenierìa:
* https://www.itexico.com/blog

### Tecnologìas:
* .NETO
* Java
* JavaScript
* DevOps
* QA

## 2
### Logotipo:
![logo3](logos/logo4.jpg)
## Nombre: GFT
### Sitio web:
* http://www.gft.com/mx
### Ubicaciòn:
* https://goo.gl/maps/UZS8yEUjJLzJoPsWA
### Acerca de:
* Empezaron como una pequeña empresa en la Selva Negra alemana y han crecido hasta convertirse en especialistas del sector financiero a nivel internacional.
* Pioneros en ofrecer servicios nearshore desde 2001 y cuentan con un equipo de 7.000 colaboradores en más de 15 países.

### Servicios:
* Inteligencia artificial
* Cloud
* Anàlisis de datos
* Greencoding

### Presencia:
* https://www.linkedin.com/company/gft-group
* https://www.facebook.com/gft.mex
* https://twitter.com/gft_mx
* https://www.instagram.com/gft_tech/
* https://www.youtube.com/user/gftgroup

### Ofertas Laborales:
* https://jobs.gft.com/Mexico/go/mexico/4412401/

### Blog de ingenierìa:
* https://blog.gft.com/es/

### Tecnologìas:
* APX
* API
* DevOps
* Android
* .net
* Java
* Aws

## 3
### Logotipo:
![logo4](logos/logo3.jpg)
## Nombre: Svitla Systems
### Sitio web:
* https://svitla.com/
### Ubicaciòn:
* https://goo.gl/maps/GPoywEmQhNZR8KYs9  

### Acerca de:
* Es una empresa probada de desarrollo de software personalizado y un proveedor de pruebas con sede en Silicon Valley. Somos su conducto hacia las innovaciones tecnológicas más punteras. Ofrecemos un valor incomparable a nuestros clientes, que confían en nuestra experiencia y muchos años de experiencia en Managed Team Extension y AgileSquads.

### Servicios:
*Proporcionar evaluaciones, planificación estratégica, consultoría y compromisos basados ​​en resultados para que los clientes ayuden a impulsar la transformación y optimización de los procesos comerciales.

### Presencia:
* https://www.facebook.com/SvitlaSystems
* https://www.linkedin.com/company/svitla-systems-inc-/?trk=biz-companies-cym
* https://www.youtube.com/channel/UC1nu2LV4_08GoZThHEindWA
* https://www.instagram.com/svitlasystems
* https://twitter.com/SvitlaSystemsIn

### Ofertas Laborales:
* https://svitla.com/career

### Blog de ingenierìa:
* https://svitla.com/blog

### Tecnologìas:
* DevOps
* Java
* Python
* Ruby
* Aws
* Node
* React

## 4
### Logotipo:
![logo5](logos/logo5.jpg)
## Nombre: HeadSpring
### Sitio web:
* https://headspring.com/
### Ubicaciòn:
* https://goo.gl/maps/sqVw3bnw5WwBqeN2A  

### Acerca de:
* Somos mucho más que un proveedor de estrategia y desarrollo de software empresarial, y los valores que impulsan a nuestra empresa no son meras palabras. Estamos motivados por nuestra oportunidad diaria de crear un impacto real en el mundo y permitir que nuestros empleados, nuestros clientes y nuestra comunidad alcancen más allá de su potencial percibido.

### Servicios:
* Servicios de consultorìa de software y desarrollo personalizado.

### Presencia:
* https://www.facebook.com/HeadspringTeam/
* https://twitter.com/headspring

### Ofertas Laborales:
* https://headspring.com/about/careers/

### Blog de ingenierìa:
* https://headspring.com/insights/our-blog/

### Tecnologìas:
* Aws
* Node
* React
* DevOps

## 5
### Logotipo:
![logo6](logos/logo6.jpg)
## Nombre: Grow IT Consulting
### Sitio web:
* http://grw.com.mx
### Ubicaciòn:
* https://goo.gl/maps/5zSQxu2sFhXj96A6A  

### Acerca de:
* Somos una empresa de desarrollo con 10 años de experiencia enfocada a productos de Microsoft Dynamics. Tenemos operaciones en México, Sudamérica y recientemente obtuvimos un reconocimiento por ser una de las mejores empresas para trabajar en software.

### Servicios:
* Servicios de desarrollo outsourcing para las soluciones Microsoft Dynamics.

### Presencia:
* https://www.facebook.com/growitc
* https://www.linkedin.com/company/grow-it-consulting
* https://www.instagram.com/grow_it_mx/
### Ofertas Laborales:
* http://www.grw.com.mx/bolsa-de-trabajo/

### Blog de ingenierìa:
* http://www.grw.com.mx/blog/

### Tecnologìas:
* Programación intermedio- avanzado
* Stored Procedures
* Vistas
* Triggers 
* POO

## 6
### Logotipo:
![logo7](logos/logo7.jpg)
## Nombre: Tango
### Sitio web:
* https://tango.io/
### Ubicaciòn:
* https://goo.gl/maps/18sGVGyxVGP95Hvr7

### Acerca de:
* Tango (anteriormente TangoSource), ayuda a empresas emergentes, medianas y empresariales innovadoras a desarrollar productos digitales impactantes a través de asociaciones apasionadas. 

### Servicios:
* Producto mínimo viable
* Aumento del personal
* Resolución de deuda técnica
* Estrategia de producto
* Escalabilidad y aceleración del producto

### Presencia:
* https://tango.io/assets/graphics/svg/social_media/sm_facebook.svg
* https://tango.io/assets/graphics/svg/social_media/sm_linkedin.svg
* https://twitter.com/tango_io
* https://tango.io/assets/graphics/svg/social_media/sm_instagram.svg

### Ofertas Laborales:
* https://jobs.lever.co/tango

### Blog de ingenierìa:
* https://blog.tango.io/

### Tecnologìas:
* RoR
* Python
* Ruby
* Aws
* Node
* React

## 7
### Logotipo:
![logo8](logos/logo8.jpg)
## Nombre: Platzi
### Sitio web:
* https://platzi.com/
### Ubicaciòn:
* https://goo.gl/maps/GGwqbXSfNU8SZdK76

### Acerca de:
* Platzi está formando el siguiente millón de profesionales en tecnología, transformando en economías digitales a países en vías de desarrollo. Mediante una plataforma y una metodología propias, que nos permiten hacer educación online efectiva enfocada en las necesidades reales de nuestra comunidad latinoamericana.

### Servicios:
* Mide y analiza los resultados de tu equipo con nuestro tablero para seguimiento.
* Acompañamiento y seguimiento por un Ejecutivo de Cuenta.
* Certificaciones co-brandeadas por cada curso del plan completado.

### Presencia:
* https://twitter.com/platzi
* https://www.youtube.com/channel/UC55-mxUj5Nj3niXFReG44OQ
* https://facebook.com/platzi
* https://www.instagram.com/platzi/

### Ofertas Laborales:
* https://platzi.com/contacto/

### Blog de ingenierìa:
* https://platzi.com/blog/

### Tecnologìas:
* React.js.
* Python/Django.
* iOS y Android

## 8
### Logotipo:
![logo9](logos/logo9.jpg)
## Nombre: Nearsoft
### Sitio web:
* https://nearsoft.com/
### Ubicaciòn:
* https://goo.gl/maps/peS1K3pNdNGjmZem9

### Acerca de:
* Ayudamos a empresas de software increíbles con el desarrollo, las pruebas y la investigación y ejecución de UX / UI de software.

### Servicios:
* Desarrollo mòvil
* Big Data
* Diseño Web
* Aplicaciones

### Presencia:
* https://twitter.com/platzi
* https://www.youtube.com/channel/UC55-mxUj5Nj3niXFReG44OQ
* https://facebook.com/platzi
* https://www.instagram.com/platzi/

### Ofertas Laborales:
* https://nearsoft.com/join-us/

### Blog de ingenierìa:
* https://nearsoft.com/blog/

### Tecnologìas:
* Node
* React
* Test
* WordPress
* DevOps
* Java
* Python
* Ruby
* Aws

## 9
### Logotipo:
![logo10](logos/logo10.jpg)
## Nombre: Aviada
### Sitio web:
* https://aviada.mx/
### Ubicaciòn:
* https://goo.gl/maps/WCfTBN4jVSjXvbE58

### Acerca de:
* Aviada es una empresa por empleados, para empleados. Creado en 2017, hemos crecido constantemente sin tener que recurrir a campañas de marketing o inversiones de terceros con una regla simple: haz lo correcto. Al final, cuando los empleados ganan, tú ganas.

### Servicios:
* Desarrollo de software
* Medios digitales
* Diseño multimedia 
* Otros.

### Presencia:
* https://www.linkedin.com/company/aviadamx/
* https://www.facebook.com/aviadamx
* https://twitter.com/AviadaMX
* https://www.instagram.com/aviadamx/


### Ofertas Laborales:
* https://careers.aviada.mx/

### Blog de ingenierìa:
* https://aviada.mx/category/blog/

### Tecnologìas:
* Ingeniería de software (Programación)
* Marketing Digital
* Gestión de Calidad (QA)
* Gestión de Proyectos (Project Management)
* Diseño Web / Interface y Experiencia de Usuarios (UI/UX)
* Diseño Gráfico
* AdOps

## 10
### Logotipo:
![logo11](logos/logo11.jpg)
## Nombre: Tacitknowledge
### Sitio web:
* https://www.tacitknowledge.com/
### Ubicaciòn:
* https://goo.gl/maps/QaKCYWJY6KTW7X8A8

### Acerca de:
* Tacit Knowledge crea, integra y respalda software empresarial para marcas globales. El enfoque principal de Tacit es el comercio digital y hemos ganado varios premios por nuestro trabajo en esta área. Nuestra experiencia internacional se extiende a implementaciones dentro del comercio móvil, comercio social y de gran escala. 

### Servicios:
* Consultorìa de Software
* Consultorìa de Ecommerce
* Comercio Digital
* Comercio SAP
* Servicios gestionados

### Presencia:
* https://twitter.com/tacitknowledge
* https://www.linkedin.com/company/tacit-knowledge


### Ofertas Laborales:
* https://www.tacitknowledge.com/careers

### Blog de ingenierìa:
* https://www.tacitknowledge.com/thoughts/

### Tecnologìas:
* AWS, GCP y Azure
* Docker y Kubernetes
* HTTP, Nginx, Apache;
* MySQL, MSSQL, PostgreSQL;
* Prometheus / Grafana, Elasticsearch / Kibana, NewRelic
* Ruby, Python;

## 11
### Logotipo:
![logo12](logos/logo12.jpg)
## Nombre: Scio
### Sitio web:
* https://www.scio.com.mx/
### Ubicaciòn:
* https://goo.gl/maps/8QoeZZWi8LLMvTrh9

### Acerca de:
* Trabaja con sus clientes para construir y operar aplicaciones y servicios confiables. Adoptan un enfoque de ciclo de vida completo, asociandose con sus clientes desde la conceptualización, pasando por el desarrollo, hasta el mantenimiento, soporte y DevOps en curso.

### Servicios:
* Desarrollo de aplicaciones web
* Desarrollo de aplicaciones SaaS
* Desarrollo de aplicaciones mòviles

### Presencia:
* https://twitter.com/sciomx
* https://www.facebook.com/ScioMx


### Ofertas Laborales:
* https://www.scio.com.mx/trabaja-scio-mexico/

### Blog de ingenierìa:
* https://www.scio.com.mx/blog/

### Tecnologìas:
* React
* Test
* WordPress
* DevOps
* Java
* Python
* Ruby

## 12
### Logotipo:
![logo13](logos/logo13.jpg)
## Nombre: Intelimetrica
### Sitio web:
* https://intelimetrica.com/
### Ubicaciòn:
* https://goo.gl/maps/QLdxn3WmSDNACgAZ8

### Acerca de:
* Intelimetrica tiene la capacidad de transformar nuestros desafíos y problemas comerciales en soluciones analíticas avanzadas. Son flexibles a nuestras necesidades comerciales y adaptan fácilmente sus soluciones para satisfacer los requisitos de nuestros clientes.

### Servicios:
* IA y ML
* Ingenierìa de software
* Diseño UX

### Presencia:
* https://twitter.com/Intelimetrica
* https://www.facebook.com/intelimetrica


### Ofertas Laborales:
* https://intelimetrica.com/careers

### Blog de ingenierìa:
* No encontrado

### Tecnologìas:
* DevOps

## 13
### Logotipo:
![logo14](logos/logo14.jpg)
## Nombre: Kata Software
### Sitio web:
* https://kata-software.com/es

### Ubicaciòn:
* https://goo.gl/maps/RAQFk9s97CUm7xvY6

### Acerca de:
* Kata Software es una empresa orientada al cliente, hemos trabajado en conjunto en el desarrollo de nuestra herramienta de campo SIMA, misma que ha cumplido nuestras expectativas, mejorando la experiencia de nuestros clientes y de nuestras fuerzas de venta.

### Servicios:
* IA
* Seguridad
* Software Contable

### Presencia:
* https://www.facebook.com/Kata-Software-109450663980021/
* https://www.linkedin.com/company/37521547/


### Ofertas Laborales:
* https://kata-software.com/es/trabaja-con-nosotros

### Blog de ingenierìa:
* https://kata-software.com/es/publicaciones

### Tecnologìas:
* Kata Builder
* Azure
* AWS 


## 14
### Logotipo:
![logo15](logos/logo15.jpg)
## Nombre: Icalia Labs

### Sitio web:
* https://www.icalialabs.com 

### Ubicaciòn:
* https://goo.gl/maps/4b8oF7pft2SzNN8V7

### Acerca de:
* Icalia Labs Es una empresa que busca transformar negocios con soluciones digitales adaptables que satisfacen las necesidades y crean oportunidades del mañana. Permitiendo a las empresas traducir sus estrategias digitales en una entrega de valor tangible y un plan de desarrollo de software de riesgo gestionado.

### Servicios
* Servicios de control de calidad y pruebas
* Transformación digital:
* Desarrollo de software
* Desarrollo de front-end
* Desarrollo de back-end


### Presencia:
*https://github.com/IcaliaLabs
*https://clutch.co/profile/icalia-labs
*https://www.linkedin.com/company/icalia-labs/

### Ofertas Laborales:
*https://www.icalialabs.com/work-design-sprint-custom-software-legacy-software


### Blog de ingenierìa:
* https://www.icalialabs.com/blog-software-development-tips-and-news

### Tecnologìas:
* .NET
* Java
* JavaScript
* HTMLS
* CSS

## 15
### Logotipo:
![logo16](logos/logo16.jpg)
## Nombre: Onephase

### Sitio web:
* https://onephase.com/

### Ubicaciòn:
* https://goo.gl/maps/qv2u8gtDC3pmGkmF9

### Acerca de:
* Onephase  se encarga de crear alianzas estratégicas que impulsa el crecimiento de la tecnología como nuestro principal impulsor ayuda las empresas a adaptarse a este mundo moderno a través de soluciones de software inteligentes a medida.

### Servicios:
*   Digital consulting
*   Dotación de personal
*   Solución a comercios electrónicos
*   Diseño UI/UX
*   Sercicio RV/RA
*   Pruebas
*   Desarrollo de software
*   Desarrollo web
*   Nube
*   Big Data

### Presencia:
* https://twitter.com/OnePhaseLLC
* https://www.facebook.com/OnePhaseLLC
* https://www.linkedin.com/in/onephasellc/
* https://www.instagram.com/onephasellc

### Ofertas Laborales:
* https://onephase.com/nearshore

### Blog de ingenierìa:
* https://onephase.com/insight

### Tecnologìas:
* Vue
* Unity
* JavaScript
* _NET
* PostgreSQL
* MySQL
* Doker


## 16
### Logotipo:
![logo17](logos/logo17.jpg)
## Nombre: Tiempo Development

### Sitio web:
* https://www.tiempodev.com 

### Ubicaciòn:
* https://goo.gl/maps/F1SVU4qKvDUSHkK46

### Acerca de:
Tiempo Development es una firma de servicios de TI  von un modelo de negocio nearshore, prácticas ágiles maduras, amplia experiencia y talento bilingüe y bicultural excepcional garantizan que ofrecemos resultados excepcionales para los clientes con cada compromiso.

### Servicios:
* Análisis técnico
* Arquitectura de software
* Diseño UI / UX
* Gestión de productos
* Mantenimiento y soporte de aplicaciones
* Pruebas y aseguramiento de la calidad
* Desarrollo de aplicaciones web
* Desarrollo de aplicaciones móviles
* Integración de aplicaciones / API
* loT
* Microservicios
* DevOps
* Pruebas de automatización
* Nativo de la nube

### Ofertas Laborales:
* https://www.3pillarglobal.com/career-opportunities/

### Blog de ingenierìa:
* https://www.tiempodev.com/blog/

### Tecnologìas:
* .NET
* Java
* JavaScript
* MySQL
* PHP

## 17
### Logotipo:
![logo18](logos/logo18.jpg)
## Nombre: MagmaLabs

### Sitio web:
* https://www.magmalabs.io/

### Ubicaciòn:
* https://www.google.com/search?q=MagmaLabs&rlz=1C1CHBF_esMX838MX838&oq=MagmaLabs&aqs=chrome..69i57j0i10j0i30l2j0i10i30.863j0j9&sourceid=chrome&ie=UTF-8#

### Acerca de:
* MagmaLabs Es una agencia de desarrollo de software. Desde el inicio, buscan como única misión impulsar la innovación mientras nos encargamos del desarrollo de nuestro talento humano a clase mundial.

### Servicios:
* Desarrollo de software
* Ruby on Rails
* Diseño de Producto Digital UI / UX
* Taller de lienzo
* Servicios de control de calidad y pruebas
* Sprint de diseño
* Sistemas de diseño
* Auditorias

### Presencia:
https://twitter.com/weareMagmaLabs
https://www.facebook.com/magmalabsio
https://www.linkedin.com/company/magmalabs
https://www.instagram.com/magmalabs/

### Ofertas Laborales:
* https://www.magmalabs.io/contact
### Blog de ingenierìa:
* http://blog.magmalabs.io/

### Tecnologìas:
* .NET
* Java
* HTML
* CSS
* JS
* MySQL

## 18
### Logotipo:
![logo19](logos/logo19.jpg)
## Nombre: Nova

### Sitio web:
* http://novasolutionsystems.com/
### Ubicaciòn:
* https://goo.gl/maps/zshiqRwE5K1pyUPR7

### Acerca de:
* Nova es una firma de servicios donde brindamos servicios de tecnología, alineados a las metas de negocio de nuestros clientes, dando servicios de alta calidad para que nos visualicen como un socio de negocio, ofreciendo de esta forma las mejores y más innovadoras soluciones de tecnología de la información para su mercado.

### Servicios:
* Desarrollo de software
* Seguridad financiera 
* Recursos digitales
* Transformación digital 

### Presencia:
* https://twitter.com/novasolutionsys/
* https://www.facebook.com/novasolutionsystems/
* https://www.linkedin.com/company/novasolutionsystems/
* https://www.instagram.com/novasolutionsystems/

### Ofertas Laborales:
* http://novasolutionsystems.com/unete/

### Blog de ingenierìa:
* No encontrado

### Tecnologìas:
* Oracle
* GitPython
* PostgreSQL
* DevOps
* QA

## 19
### Logotipo:
![logo20](logos/logo20.jpg)
## Nombre: Enroute System

### Sitio web:
* https://enroutesystems.com/

### Ubicaciòn:
* https://goo.gl/maps/RUm3ECSoqfaXfFAc8

### Acerca de:
* Enroute se trata de ser excepcional. Brindamos servicios y soluciones de TI proporcionados por un equipo de personas apasionadas en la resolución de problemas, altamente capacitadas en diferentes prácticas comerciales y de TI.

### Servicios:
* Ingenierìa de calidad
* Desarrollo de Software
* Ingenierìa de Datos

### Presencia:
* https://www.linkedin.com/company/enroutesystems
* https://www.facebook.com/enroutesystems
* https://www.instagram.com/enroutesystems/


### Ofertas Laborales:
* https://enroutesystems.com/careers

### Blog de ingenierìa:
* No encontrado

### Tecnologìas:
* React
* Java
* Python
* Ruby
* Node
* HTML5
* Java Script

## 20
### Logotipo:
![logo21](logos/logo21.jpg)
## Nombre: Roomie IT

### Sitio web:
* https://www.roomie-it.org/

### Ubicaciòn:
* https://g.page/Roomie-it?share

### Acerca de:
* Roomie IT es una firma de servicios de TI en especifico la primera empresa latinoamericana de robótica que busca transformar organizaciones a través de robots de servicio.

### Servicios:
* Robótica
* IA
* Desarrollo de software
* Servicios de control de calidad y pruebas
* Soluciones de Nube

### Presencia:
https://twitter.com/IT_ROOMIE
https://www.facebook.com/roomieit/
https://www.linkedin.com/company/roomie-it

### Ofertas Laborales:
* https://www.roomie-it.org/careers

### Blog de ingenierìa:
* https://www.roomie-it.org/blog

### Tecnologìas:
* Rubí
* Java
* Python
* PHP
* JS
